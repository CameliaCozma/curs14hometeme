package com.sda.home.list;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RulareCod {
public static void main(String[] args) throws InterruptedException {
    Map<String, Integer> map = new HashMap<>();

    map.put("pink", 100);
    map.put("lila", 180);
    map.put("deep pink", 196);
    map.put("light coral", 200);
    map.put("hot pink", 259);

    Masina m1 = new Masina("pink", 100);
    Masina m2 = new Masina("lila", 180);
    Masina m3 = new Masina("deep pink", 196);
    Masina m4 = new Masina("light coral", 200);
    Masina m5 = new Masina("hot pink", 259);

    map.put(String.valueOf(m1), 1);
    map.put(String.valueOf(m2), 2);
    map.put(String.valueOf(m3), 3);
    map.put(String.valueOf(m4), 4);
    map.put(String.valueOf(m5), 5);

    Set<String > masini = map.keySet();
    for (String m : masini ) {
        System.out.println(m);
        System.out.println(" - " + map.get(m));
    }


}
}

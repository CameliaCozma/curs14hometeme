package com.sda.home.list;

public class Masina {
    private String color;
    private int vitezaMaxima;

public Masina(String color,int vitezaMaxima) {
    this.color = color;
    this.vitezaMaxima = vitezaMaxima;
}

public String getColor() {
    return color;
}

public void setColor(String color) {
    this.color = color;
}

public int getVitezaMaxima() {
    return vitezaMaxima;
}

public void setVitezaMaxima(int vitezaMaxima) {
    this.vitezaMaxima = vitezaMaxima;
}

@Override
public String toString() {
    return "Masina{" +
            "color='" + color + '\'' +
            ", vitezaMaxima=" + vitezaMaxima +
            '}';
}
}

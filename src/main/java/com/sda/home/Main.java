package com.sda.home;

public class Main {
public static void main(String[] args) {
    String str = "abcaab";
    String reversedString;
    reversedString = " ";

    for (int i = str.length()-1; i >= 0 ; i--) {
        reversedString = reversedString + str.charAt(i);
        System.out.println("Reversed string is: " + reversedString);

        if (reversedString == str){
            System.out.println("The string is palindrome");
        } else {
            System.out.println("The string is not palindrome");
        }
    }
}
}

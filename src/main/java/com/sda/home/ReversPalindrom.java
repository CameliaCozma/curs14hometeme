package com.sda.home;

public class ReversPalindrom {
    private String str = "abcaab";
    private String reversedString;

public ReversPalindrom(String str,String reversedString) {
    this.str = str;
    this.reversedString = reversedString;
}

public String getStr() {
    return str;
}

public void setStr(String str) {
    this.str = str;
}

public String getReversedString() {
    return reversedString;
}

public void setReversedString(String reversedString) {
    this.reversedString = reversedString;
}

@Override
public String toString() {
    return "ReversPalindrom{" +
            "str='" + str + '\'' +
            ", reversedString='" + reversedString + '\'' +
            '}';
}
}

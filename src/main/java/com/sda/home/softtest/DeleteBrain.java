package com.sda.home.softtest;

public class DeleteBrain {
    private String str = "abacaa";

public DeleteBrain(String str) {
    this.str = str;
}

public String getStr() {
    return str;
}

public void setStr(String str) {
    this.str = str;
}

@Override
public String toString() {
    return "DeleteBrain{" +
            "str='" + str + '\'' +
            '}';
}
}
